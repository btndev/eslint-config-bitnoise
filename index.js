module.exports = {
  extends: [
    'eslint-config-airbnb-base',
    'eslint-config-airbnb-base/rules/strict',
    'eslint-config-standard',
    './rules',
  ].map(require.resolve).concat([
    'plugin:jest/recommended',
  ]),
  plugins: [
    'standard',
    'jest',
  ],
  env: {
    jest: true,
    'jest/globals': true,
  },
}
