module.exports = {
  extends: [
  ],
  plugins: [
    'filenames',
    'jsdoc',
    'unicorn',
  ],
  parserOptions: {
    ecmaVersion: 2016,
    sourceType: 'module',
  },
  env: {
    browser: true,
    node: true,
    es6: true,
    mocha: true,
    commonjs: true,
    jest: true,
  },
  rules: {
    'arrow-parens': ['error', 'always'],
    'arrow-body-style': ['error', 'as-needed'],
    'comma-dangle': ['error', 'always-multiline'],
    'no-console': 'error',
    'prefer-template': 'error',
    'class-methods-use-this': 0,
    'newline-before-return': 'error',
    'object-curly-newline': [
      'error',
      {
        ObjectExpression: {
          multiline: true,
          consistent: true,
          minProperties: 5,
        },
        ObjectPattern: {
          multiline: true,
          consistent: true,
          minProperties: 5,
        },
      },
    ],
    'no-underscore-dangle': [
      'error',
      {
        allow: [
          '_error',
        ],
      },
    ],
    'id-length': ['error'],
    'max-lines': [
      'error',
      {
        max: 300,
        skipBlankLines: true,
        skipComments: true,
      },
    ],
    'max-len': [
      'error',
      {
        code: 120,
        ignoreComments: true,
        ignoreUrls: true,
        ignoreRegExpLiterals: true,
        ignoreStrings: true,
        ignoreTemplateLiterals: true,
      },
    ],
    'no-plusplus': [
      'error',
      {
        allowForLoopAfterthoughts: true,
      },
    ],
    'func-style': [
      'error',
      'declaration',
      {
        allowArrowFunctions: true,
      },
    ],
    'func-names': ['error', 'as-needed'],
    'no-unused-vars': [
      'error',
      {
        vars: 'all',
        ignoreRestSiblings: true,
        args: 'after-used',
        argsIgnorePattern: '^ignore',
        caughtErrors: 'all',
        caughtErrorsIgnorePattern: '^ignore',
      },
    ],
    'operator-linebreak': [
      'error',
      'after',
      {
        overrides: {
          '?': 'before',
          ':': 'before',
        },
      },
    ],
    'filenames/match-exported': 'error',
    'import/max-dependencies': [
      'error',
      {
        max: 10,
      },
    ],
    'import/no-anonymous-default-export': [
      'error',
      {
        allowArray: false,
        allowArrowFunction: false,
        allowAnonymousClass: false,
        allowAnonymousFunction: false,
        allowLiteral: false,
        allowObject: false,
      },
    ],
    'jsdoc/check-param-names': 'error',
    'jsdoc/check-tag-names': 'error',
    'jsdoc/check-types': 'error',
    'jsdoc/newline-after-description': 'error',
    'jsdoc/require-description-complete-sentence': 'error',
    'jsdoc/require-hyphen-before-param-description': 'error',
    'jsdoc/require-param': 'error',
    'jsdoc/require-param-description': 'error',
    'jsdoc/require-param-type': 'error',
    'jsdoc/require-returns-description': 'error',
    'jsdoc/require-returns-type': 'error',
    'unicorn/catch-error-name': [
      'error',
      {
        name: 'error',
      },
    ],
    'unicorn/explicit-length-check': 'error',
    'unicorn/no-abusive-eslint-disable': 'error',
    'unicorn/no-process-exit': 'error',
    'unicorn/throw-new-error': 'error',
    'unicorn/number-literal-case': 'error',
    'unicorn/escape-case': 'error',
    'unicorn/no-array-instanceof': 'error',
    'unicorn/no-new-buffer': 'error',
    'unicorn/no-hex-escape': 'error',
    'unicorn/custom-error-definition': 'error',
    'unicorn/prefer-starts-ends-with': 'error',
    'unicorn/prefer-type-error': 'error',
  },
}
